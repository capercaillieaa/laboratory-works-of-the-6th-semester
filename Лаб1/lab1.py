import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from math import sqrt

def distance(x1, x2, y1, y2, z1, z2):
    return sqrt((x1-x2)**2 + (y1-y2)**2 + (z1-z2)**2)**3

def get_speed(v):
    return v

def get_first_body_xTrajectory(t, params):
    return -(m2 * (params[0]-params[1])/distance(params[0], params[1], params[3], params[4], params[6], params[7]) + 
    m3 * (params[0]-params[2])/distance(params[0], params[2], params[3], params[5], params[6], params[8]))

def get_first_body_yTrajectory(t, params):
    return -(m2 * (params[3]-params[4])/distance(params[0], params[1], params[3], params[4], params[6], params[7]) + 
    m3 * (params[3]-params[5])/distance(params[0], params[2], params[3], params[5], params[6], params[8]))

def get_first_body_zTrajectory(t, params):
    return -(m2 * (params[6]-params[7])/distance(params[0], params[1], params[3], params[4], params[6], params[7]) + 
    m3 * (params[6]-params[8])/distance(params[0], params[2], params[3], params[5], params[6], params[8]))

def get_second_body_xTrajectory(t, params):
    return -(m1 * (params[1]-params[0])/distance(params[1], params[0], params[4], params[3], params[7], params[6]) + 
    m3 * (params[1]-params[2])/distance(params[1], params[2], params[4], params[5], params[7], params[8]))

def get_second_body_yTrajectory(t, params):
    return -(m1 * (params[4]-params[3])/distance(params[1], params[0], params[4], params[3], params[7], params[6]) + 
    m3 * (params[4]-params[5])/distance(params[1], params[2], params[4], params[5], params[7], params[8]))

def get_second_body_zTrajectory(t, params):
    return -(m1 * (params[7]-params[6])/distance(params[1], params[0], params[4], params[3], params[7], params[6]) + 
    m3 * (params[7]-params[8])/distance(params[1], params[2], params[4], params[5], params[7], params[8]))

def get_third_body_xTrajectory(t, params):
    return -(m1 * (params[2]-params[0])/distance(params[2], params[0], params[5], params[3], params[8], params[6]) + 
    m2 * (params[2]-params[1])/distance(params[2], params[1], params[5], params[4], params[8], params[7]))

def get_third_body_yTrajectory(t, params):
    return -(m1 * (params[5]-params[3])/distance(params[2], params[0], params[5], params[3], params[8], params[6]) + 
    m2 * (params[5]-params[4])/distance(params[2], params[1], params[5], params[4], params[8], params[7]))

def get_third_body_zTrajectory(t, params):
    return -(m1 * (params[8]-params[6])/distance(params[2], params[0], params[5], params[3], params[8], params[6]) + 
    m2 * (params[8]-params[7])/distance(params[2], params[1], params[5], params[4], params[8], params[7]))

def runge_cutta(h, t, speed_getter, trajectory_getters, coordinate_params, v_params):
    k1_params = [speed_getter(v) for v in v_params]
    l1_params = [get_trajectory(t, coordinate_params) for get_trajectory in trajectory_getters]

    k2_params = [h * speed_getter(v + l1/2) for v, l1 in zip(v_params, l1_params)]
    l2_params = [h * get_trajectory(t + h/2, [coordinate_param + k1/2 for coordinate_param, k1 in zip(coordinate_params, k1_params)]) 
        for get_trajectory in trajectory_getters]
    
    k3_params = [h * speed_getter(v + l2/2) for v, l2 in zip(v_params, l2_params)]
    l3_params = [h * get_trajectory(t + h/2, [coordinate_param + k2/2 for coordinate_param, k2 in zip(coordinate_params, k2_params)]) 
        for get_trajectory in trajectory_getters]
    
    k4_params = [h * speed_getter(v + l3) for v, l3 in zip(v_params, l3_params)]
    l4_params = [h * get_trajectory(t + h, [coordinate_param + k3 for coordinate_param, k3 in zip(coordinate_params, k3_params)]) 
        for get_trajectory in trajectory_getters]

    coordinate_params = [x + (k1_params[i] + 2 * k2_params[i] + 2 * k3_params[i] + k4_params[i])/6 for x, i in zip(coordinate_params, range(9))]
    v_params = [v + (l1_params[i] + 2 * l2_params[i] + 2 * l3_params[i] + l4_params[i])/6 for v, i in zip(v_params, range(9))]

    return coordinate_params, v_params

def main():
    step = 0.001
    coordinate_params = [x10, x20, x30, y10, y20, y30, z10, z20, z30]
    v_params = [vx10, vx20, vx30, vy10, vy20, vy30, vz10, vz20, vz30]
    trajectory_getters = [get_first_body_xTrajectory, get_second_body_xTrajectory, get_third_body_xTrajectory,
                        get_first_body_yTrajectory, get_second_body_yTrajectory, get_third_body_yTrajectory,
                        get_first_body_zTrajectory, get_second_body_zTrajectory, get_third_body_zTrajectory]
    
    t_values = [i * step for i in range(100001)]
    coordinate_values = list()
    for parametr in coordinate_params:
        coordinate_values.append([parametr])

    coordinate_params, v_params = runge_cutta(step, t_values[0], get_speed, trajectory_getters, coordinate_params, v_params)
    for (i, parametr) in zip(range(len(coordinate_values)), coordinate_params):
        coordinate_values[i].append(parametr)

    for i in range(1, 100000):
        coordinate_params, v_params = runge_cutta(step, t_values[i],  get_speed, trajectory_getters, coordinate_params, v_params)
        for (j, parametr) in zip(range(len(coordinate_values)), coordinate_params):
            coordinate_values[j].append(parametr)
    
    figure = plt.figure()
    axes = Axes3D(figure)
    axes.plot(coordinate_values[0], coordinate_values[3], coordinate_values[6])
    axes.plot(coordinate_values[1], coordinate_values[4], coordinate_values[7])
    axes.plot(coordinate_values[2], coordinate_values[5], coordinate_values[8])
    plt.show()
    
print("Введите параметры первого тела (m1, x10, y10, z10, vx10, vy10, vz10):")
m1, x10, y10, z10, vx10, vy10, vz10 = (float(value) for value in input().split())
print("Введите параметры второго тела (m2, x20, y20, z20, vx20, vy20, vz20):")
m2, x20, y20, z20, vx20, vy20, vz20 = (float(value) for value in input().split()) 
print("Введите параметры третьего тела (m3, x30, y30, z30 vx30, vy30, vz30):")
m3, x30, y30, z30, vx30, vy30, vz30 = (float(value) for value in input().split()) 
main()