import matplotlib.pyplot as plt

def func1(z):
    return z

def func2(t = 0, x = 0, z = 0):
    return -4 * x

def runge_cutta(h, t = 0, x = 0, z = 0):
    k1 = h * func1(z)
    l1 = h * func2(x = x)

    k2 = h * func1(z + l1/2)
    l2 = h * func2(x = x + k1/2)

    k3 = h * func1(z + l2/2)
    l3 = h * func2(x = x + k2/2)

    k4 = h * func1(z + l3)
    l4 = h * func2(x = x + k3)

    x += (k1 + 2 * k2 + 2 * k3 + k4)/6
    z += (l1 + 2 * l2 + 2 * l3 + l4)/6
    return x, z


def main():
    z0 = 2
    x0 = 2
    h = 0.1
    t_list = [i/10 for i in range(201)]
    x_list = [x0]
    z_list = [z0]

    x, z = runge_cutta(h, x = x0, z = z0)
    x_list.append(x)
    z_list.append(z)
    for i in range(1, 201):
        x, z = runge_cutta(h, x = x, z = z)
        x_list.append(x)
        z_list.append(z)

    plt.plot(t_list, x_list)
    plt.show()
    plt.plot(x_list, z_list)
    plt.show()
    
main()