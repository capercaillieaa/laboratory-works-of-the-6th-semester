import matplotlib.pyplot as plt
from math import sin

def func1(t, x1, x2):
    return x1 * (-0.7 + 0.5 * x2)

def func2(t, x1, x2):
    return x2 * (3 - 2 * x1)

def runge_cutta(h, t, x1, x2):
    k1 = h * func1(t, x1, x2)
    l1 = h * func2(t, x1, x2)

    k2 = h * func1(t + h/2, x1 + k1/2, x2 + l1/2)
    l2 = h * func2(t + h/2, x1 + k1/2, x2 + l1/2)

    k3 = h * func1(t + h/2, x1 + k2/2, x2 + l2/2)
    l3 = h * func2(t + h/2, x1 + k2/2, x2 + l2/2)

    k4 = h * func1(t + h, x1 + k3, x2 + l3)
    l4 = h * func2(t + h, x1 + k3, x2 + l3)

    x1 += (k1 + 2 * k2 + 2 * k3 + k4)/6
    x2 += (l1 + 2 * l2 + 2 * l3 + l4)/6
    return x1, x2

def main():
    x10 = 3
    x20 = 3
    h = 0.1
    t_list = [i/10 for i in range(201)]
    x1_list = [x10]
    x2_list = [x20]

    x1, x2 = runge_cutta(h, t_list[0], x10, x20)
    x1_list.append(x1)
    x2_list.append(x2)
    for i in range(1, 200):
        x1, x2 = runge_cutta(h, t_list[i], x1, x2)
        x1_list.append(x1)
        x2_list.append(x2)

    plt.plot(t_list, x1_list, "b")
    plt.plot(t_list, x2_list, "r")
    plt.xlabel("t")
    plt.ylabel("x = f(t)")
    plt.show()

    plt.plot(x1_list, x2_list)
    plt.xlabel("x1")
    plt.ylabel("x2 = g(x1)")
    plt.show()
    
main()